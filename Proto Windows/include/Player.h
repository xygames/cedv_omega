#pragma once
#include <iostream>
#include <string>
#include <Ogre.h>
#include <OIS/OIS.h>
#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>

class Player
{
public:
	Player(Ogre::SceneManager* Mgr, OgreBulletDynamics::DynamicsWorld * _world, Ogre::Camera* camera_player, std::string s);
	~Player(void);
	void ActualizaDirection(void);
	btVector3 getpositionbody(void);
	Ogre::Camera* getCamera(void);
	Ogre::Vector3 getdirection(void);

	void Estabilizar(void);

	
	void ImpulseShip(Ogre::Vector3 v);
	void RotateShip(const OIS::MouseEvent &e);

private:
	Ogre::SceneNode* nodeplayer;
	Ogre::Camera* camera;
	OgreBulletCollisions::CollisionShape *playerShape;
	OgreBulletDynamics::RigidBody *rigidplayer;
	int fuel;

};

