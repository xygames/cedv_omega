#include "PlayState.h"
#include "PauseState.h"

#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"	
#include "Shapes/OgreBulletCollisionsSphereShape.h"	
#include "Shapes/OgreBulletCollisionsCylinderShape.h"		
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "OgreBulletCollisionsRay.h"








template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void
PlayState::enter ()
{
  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  camera_player = _sceneMgr->getCamera("IntroCamera");
  camera_enemy = _sceneMgr->createCamera("SecondCamera");
  camera_player->setPosition(Ogre::Vector3(0,20,50));
  camera_player->lookAt(Ogre::Vector3(0,0,-1));

  //camera_player->setOrientation(Ogre::Quaternion(4,1,0,0));
  camera_player->setNearClipDistance(1);
  camera_player->setFarClipDistance(10000);
  _viewport = _root->getAutoCreatedWindow()->addViewport(camera_player);
  // Nuevo background colour.
  //_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));

////////////
  _numEntities = 0;    // Numero de Shapes instanciadas
  _timeLastObject = 0; // Tiempo desde que se añadio el ultimo objeto
 
  // Creacion del modulo de debug visual de Bullet ------------------
  _debugDrawer = new OgreBulletCollisions::DebugDrawer();
  _debugDrawer->setDrawWireframe(true);	 
  Ogre::SceneNode *node = _sceneMgr->getRootSceneNode()->
    createChildSceneNode("debugNode", Ogre::Vector3::ZERO);
  node->attachObject(static_cast <Ogre::SimpleRenderable *>(_debugDrawer));

  // Creacion del mundo (definicion de los limites y la gravedad) ---
  Ogre::AxisAlignedBox worldBounds = Ogre::AxisAlignedBox (
    Ogre::Vector3 (-10000, -10000, -10000), 
    Ogre::Vector3 (10000,  10000,  10000));
  Ogre::Vector3 gravity = Ogre::Vector3(0, -9.8, 0);

  _world = new OgreBulletDynamics::DynamicsWorld(_sceneMgr,
 	   worldBounds, gravity);
  _world->setDebugDrawer (_debugDrawer);

/////////////////OIS/////////////
  _key_pressed= false;

//////////////////////////////////////
  createScene();
  _exitGame = false;
}


void PlayState::createScene() {


  //-------------------------ESTRELLAS----------------------------


  _sceneMgr->setSkyBox(true, "SkySpace");





  

  //---------------------NAVE------------------------------


  Jugador = new Player(_sceneMgr, _world, camera_player, "Nave2");

//---------------------NAVE ENEMIGA------------------------------


  flecha2 = _sceneMgr->createEntity("Nave1.mesh");
  nodeflecha2 = _sceneMgr->createSceneNode("Flecha2");
  nodeflecha2->attachObject(flecha2);
  _sceneMgr->getRootSceneNode()->addChild(nodeflecha2);

  nodeflecha2->setScale(0.7,0.7,0.7);
  

  nodeflecha2->attachObject(camera_enemy);

camera_enemy->setPosition(0,0,-20);

  OgreBulletCollisions::CollisionShape *flechaShape2 =new OgreBulletCollisions::BoxCollisionShape(flecha2->getBoundingBox().getSize()/3.0f);


  rigidflecha2 = new OgreBulletDynamics::RigidBody("flechaRigid2", _world);
  rigidflecha2->setShape(nodeflecha2, flechaShape2, 0, 0.6, 1,   Ogre::Vector3(0,20,-50), Ogre::Quaternion::IDENTITY);

  rigidflecha2->getBulletRigidBody()->setGravity(btVector3 (0,0,0));
  rigidflecha2->disableDeactivation();
/*
 pos = rigidflecha2->getBulletRigidBody()->getCenterOfMassPosition ();

 trans = btTransform(orientacion,pos);
  rigidflecha2->getBulletRigidBody()->proceedToTransform(trans); 
*/
}



void
PlayState::exit ()
{
  _sceneMgr->clearScene();
  _root->getAutoCreatedWindow()->removeAllViewports();

  // Eliminar cuerpos rigidos --------------------------------------
  std::deque <OgreBulletDynamics::RigidBody *>::iterator 
     itBody = _bodies.begin();
  while (_bodies.end() != itBody) {   
    delete *itBody;  ++itBody;
  } 
 
  // Eliminar formas de colision -----------------------------------
  std::deque<OgreBulletCollisions::CollisionShape *>::iterator 
    itShape = _shapes.begin();
  while (_shapes.end() != itShape) {   
    delete *itShape; ++itShape;
  } 

  _bodies.clear();  _shapes.clear();

  // Eliminar mundo dinamico y debugDrawer -------------------------
  delete _world->getDebugDrawer();    _world->setDebugDrawer(0);
  delete _world;


}

void
PlayState::pause()
{
  _pick=false;
}

void
PlayState::resume()
{
  _pick=true;
  // Se restaura el background colour.
  _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
}

bool
PlayState::frameStarted
(const Ogre::FrameEvent& evt)
{
  Ogre::Vector3 vt(0,0,0);     Ogre::Real tSpeed = 20.0;  

  Ogre::Real deltaT = evt.timeSinceLastFrame;
  int fps = 1.0 / deltaT;

  _world->stepSimulation(deltaT, 1); // Actualizar simulacion Bullet



  Jugador->ActualizaDirection();
  camera_enemy->lookAt(rigidflecha2->getCenterOfMassPosition());

direc=Jugador->getpositionbody()-rigidflecha2->getBulletRigidBody()->getCenterOfMassPosition();
  force+= deltaT;

angle=direc.angle (btVector3(camera_enemy->getDerivedDirection().x,camera_enemy->getDerivedDirection().y,camera_enemy->getDerivedDirection().z));


  MoverEnemigo();






  return true;
}

bool
PlayState::frameEnded
(const Ogre::FrameEvent& evt)
{
  Ogre::Real deltaT = evt.timeSinceLastFrame;
  if (_exitGame)
    return false;
    _world->stepSimulation(deltaT); // Actualizar simulacion Bullet
  return true;
}

void
PlayState::keyPressed
(const OIS::KeyEvent &e)
{
  Ogre::Real deltaT = 0.02;

  // Tecla p --> PauseState.
  if (e.key == OIS::KC_P) {
    pushState(PauseState::getSingletonPtr());
  }
  if (e.key == OIS::KC_SPACE) {
    std::cout<< "anlge " <<direc.angle (btVector3(camera_enemy->getDerivedDirection().x,camera_enemy->getDerivedDirection().y,camera_enemy->getDerivedDirection().z))<<std::endl;
    //std::cout<<rigidflecha->getBulletRigidBody()->getOrientation().getAngle()<<std::endl;
    std::cout<<"PLAYER: "<<Jugador->getdirection().x<<","<<Jugador->getdirection().y<<","<<Jugador->getdirection().z<<std::endl;
std::cout<<"ENEMY: "<<camera_enemy->getDerivedDirection().x<<","<<camera_enemy->getDerivedDirection().y<<","<<camera_enemy->getDerivedDirection().z<<std::endl;
  }
    if (e.key == OIS::KC_R){

		Jugador->Estabilizar();
  }

    if (e.key == OIS::KC_E){
	Girar();
    }
}

void
PlayState::keyReleased
(const OIS::KeyEvent &e)
{
  if (e.key == OIS::KC_ESCAPE) {
    _exitGame = true;
  }

}

void
PlayState::mouseMoved
(const OIS::MouseEvent &e)
{

if (_key_pressed){

	Jugador->RotateShip(e);

  }
}

void
PlayState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  if(id==OIS::MB_Left){
    _key_pressed = true;
    _mouse_position = e.state;
  }

}

void
PlayState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  if(id == OIS::MB_Left)
     _key_pressed=false;
  if(id==OIS::MB_Right){
	  Jugador->ImpulseShip(Jugador->getdirection());
  }

}

PlayState*
PlayState::getSingletonPtr ()
{
return msSingleton;
}

PlayState&
PlayState::getSingleton ()
{ 
  assert(msSingleton);
  return *msSingleton;
}

void
PlayState::MoverEnemigo()
{


Girar();
btVector3 velocidad = btVector3(camera_enemy->getDerivedDirection().x,camera_enemy->getDerivedDirection().y,camera_enemy->getDerivedDirection().z);

velocidad=3*velocidad.normalize();

rigidflecha2->getBulletRigidBody()->setLinearVelocity(velocidad);


}


void
PlayState::Girar()
{
if(angle>0.1 && angle<3){
  btVector3 pos2 = rigidflecha2->getBulletRigidBody()->getCenterOfMassPosition (); 
   btQuaternion orientacion2 = rigidflecha2->getBulletRigidBody()->getOrientation();
   orientacion2= orientacion2 * btQuaternion(direc.cross(btVector3(camera_enemy->getDerivedDirection().x,camera_enemy->getDerivedDirection().y,camera_enemy->getDerivedDirection().z)),-0.01);
   btTransform trans2 = btTransform(orientacion2,pos2);
   rigidflecha2->getBulletRigidBody()->proceedToTransform(trans2);}
/*if(angle>3){
  btVector3 pos2 = rigidflecha2->getBulletRigidBody()->getCenterOfMassPosition (); 
   btQuaternion orientacion2 = rigidflecha2->getBulletRigidBody()->getOrientation();
   orientacion2= orientacion2 * btQuaternion(direc.cross(btVector3(camera_enemy->getDerivedDirection().x,camera_enemy->getDerivedDirection().y,camera_enemy->getDerivedDirection().z)),-angle);
   btTransform trans2 = btTransform(orientacion2,pos2);
   rigidflecha2->getBulletRigidBody()->proceedToTransform(trans2);}
*/
}
