#include "Player.h"


Player::Player(Ogre::SceneManager* Mgr, OgreBulletDynamics::DynamicsWorld * _world, Ogre::Camera* camera_player, std::string s)
{
	//CREACION EN OGRE
	Ogre::Entity* player = Mgr->createEntity(s+".mesh");
	nodeplayer = Mgr->createSceneNode("Player");
	nodeplayer->attachObject(player);
	Mgr->getRootSceneNode()->addChild(nodeplayer);
	nodeplayer->setScale(0.7,0.7,0.7);
	nodeplayer->attachObject(camera_player);
    camera_player->setPosition(0,0,30);

	//A�ADIENDO FISICA
	playerShape = new OgreBulletCollisions::BoxCollisionShape(player->getBoundingBox().getSize()/3.0f);
	rigidplayer = new OgreBulletDynamics::RigidBody("playerRigid", _world);
	rigidplayer->setShape(nodeplayer, playerShape, 0, 0.6, 1,   Ogre::Vector3(0,20,20), Ogre::Quaternion::IDENTITY);
	rigidplayer->getBulletRigidBody()->setGravity(btVector3 (0,0,0));
    rigidplayer->disableDeactivation();
	//
	camera_player->lookAt(rigidplayer->getCenterOfMassPosition());
	camera=camera_player;

	fuel=100;
}


Player::~Player(void)
{
	delete rigidplayer;
	delete playerShape;
	//nodeplayer->removeAndDestroyAllChildren();
}

void Player::ActualizaDirection(void)
{
	camera->lookAt(rigidplayer->getCenterOfMassPosition());;
}

Ogre::Camera* Player::getCamera(void)
{
	return camera;
}

btVector3 Player::getpositionbody(void)
{
	return rigidplayer->getBulletRigidBody()->getCenterOfMassPosition();
}

Ogre::Vector3 Player::getdirection(void){
	return camera->getDerivedDirection();
}

void Player::Estabilizar(void)
{
	rigidplayer->getBulletRigidBody()->setLinearVelocity(btVector3 (0,0,0));
    rigidplayer->getBulletRigidBody()->setAngularVelocity(btVector3 (0,0,0));
}

void Player::ImpulseShip(Ogre::Vector3 v)
{
	rigidplayer->getBulletRigidBody()->setLinearVelocity(rigidplayer->getBulletRigidBody()->getLinearVelocity() + btVector3(v.x,v.y,v.z));
    if(rigidplayer->getBulletRigidBody()->getLinearVelocity().length ()>5){
      rigidplayer->getBulletRigidBody()->setLinearVelocity(5*rigidplayer->getBulletRigidBody()->getLinearVelocity().normalized());
    }
}

void Player::RotateShip(const OIS::MouseEvent &e)
{
	btQuaternion orientacion = rigidplayer->getBulletRigidBody()->getOrientation();
    orientacion= orientacion * btQuaternion((- e.state.X.rel*0.02)*0.1,(- e.state.Y.rel*0.02)*0.1,0);
    btTransform trans = btTransform(orientacion,rigidplayer->getBulletRigidBody()->getCenterOfMassPosition());
    rigidplayer->getBulletRigidBody()->proceedToTransform(trans);
}