
#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>




#include "GameState.h"

#define LADRILLO 1 << 1

class PlayState : public Ogre::Singleton<PlayState>, public GameState
{
 public:
  PlayState () {}

  void enter ();
  void createScene();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  void DetectCollision();
  void MoverEnemigo();
  void Girar();

  // Heredados de Ogre::Singleton.
  static PlayState& getSingleton ();
  static PlayState* getSingletonPtr ();

 protected:


btVector3 direc;
btScalar angle;
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;
  Ogre::Camera* _camera2;
  Ogre::SceneNode* _node;
  Ogre::Entity* ball;
  Ogre::SceneNode* _ballNode;
  Ogre::SceneNode* _ballNode2;
  Ogre::SceneNode* nodeflecha;
  Ogre::SceneNode* nodeflecha2;
  Ogre::Entity* flecha;
  Ogre::Entity* flecha2;
OgreBulletDynamics::RigidBody *rigidflecha;
OgreBulletDynamics::RigidBody *rigidflecha2;
OgreBulletCollisions::TriangleMeshCollisionShape *Trimesh2;

  OgreBulletDynamics::DynamicsWorld * _world;
  OgreBulletCollisions::DebugDrawer * _debugDrawer;
  int _numEntities;
  float _timeLastObject;

  std::deque <OgreBulletDynamics::RigidBody *>         _bodies;
  std::deque <OgreBulletCollisions::CollisionShape *>  _shapes;

Ogre::Vector3 vel;
Ogre::Vector3 vel2;
int forcebool;
Ogre::Real force;
Ogre::Vector3 posobjetomov;
Ogre::Vector3 auxcamera1;
Ogre::Vector3 auxcamera2;
   Ogre::Quaternion ori;
  OgreBulletDynamics::RigidBody *objetomov;

  bool _exitGame;

 private:
  Ogre::Ray setRayQuery(int posx, int posy, int mask);
  OIS::MouseState _mouse_position;
  bool _key_pressed;
  bool _end_game;
  bool _pick;

  
};

#endif
